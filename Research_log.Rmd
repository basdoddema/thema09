---
title: "Thema 09"
author: "Bas Doddema"
date: "9/12/2019"
output: pdf_document
---

```{r setup, include=FALSE}
# Load all libraries
knitr::opts_chunk$set(echo = TRUE, cache = TRUE)
library(ggplot2)
library(tidyr)
library(knitr)
library(gridExtra)
library(scales)
```

## 9-12-2019

# Original dataset
The original dataset was published in 2009 and contains red and white wine samples from the the Portuguese "Vinho Verde" wine. The data for this dataset were collected from may 2004 to february 2007. The main purpose of the original research was to predict wine taste preferences, based on analytical data.

For my research I will be using the red wine samples only. This dataset contains 4898 instances of which 1599 red wine instances, and 12 attributes. The class atrribute is 'quality' and gives the instance a grade from 0 to 10 to indicate the quality of the wine. This wine quality is determined by the physicochemical properties of that particular wine. 

# Research question
The main goal of this research is to apply machine learning techniques to predict wine quality based on multiple physicochemical properties.

## 9-17-2019

# Loading data and EDA

For this week I am going to load the data and perform some Exploratory data analysis(EDA). To do that I will make some tables and figures with the ggplot2 package. There are a number of things I will look at during the EDA, these include:  
* Is there variation in the data?
* Are there missing data?
* Is there any correlation in the data?
* Which attributes are the most interesting for this research? 

```{r Load data}
# Load file
wine.data <- read.table("winequality-red.csv", header=T, sep=";")
str(data)
```

```{r Summary}
# Create summary of complete dataset
summary(wine.data)

# Create summary of total sulfur dioxide
summary(wine.data$total.sulfur.dioxide)
```

A first look at the summary of the data shows that there are some unexpected values and possibly outliers. For example, the amount of total sulfur dioxide contains a value of 289, which looks a bit high compared to the mean and median. The summary also shows that there are no NA values in the data.


```{r Load codebook}
# Load codebook
codebook <- read.table("winequality-codebook.csv", header=T, sep=",")
names(wine.data) <- codebook[,2]

```


```{r Histogram quality, fig.height = 3, fig.width = 4}
# create histogram with quality scores
ggplot(wine.data, aes(x=quality)) + geom_histogram(binwidth = 0.25) +
  xlab("Quality score") + ylab("Frequency")
```

## 24-09-2019
# Recoding attributes
In the original research they used a quality score between 0 and 10, but there isn't an explanation about what makes a wine bad, average or good. First I made a histogram with the amounts of quality scores of the whole dataset, in there you can see that there are not many wines that score a 3, 4, 7 or 8 and the most common are 5 and 6. In my opinion a wine that scores lower than a 5, can be named a bad wine. A wine that scores between 5 and 7, can be named an average wine and a wine that scores higher than a 7 can be named a good wine. With these quality groups I can determine if a wine is bad, good or average, instead of giving a score between 0 or 10.

```{r change class attribute 1}
# reformat quality attribute to 3 values
quality.factor <- cut(wine.data$quality, 
               breaks = c(0,5,7,10), 
               labels = c("Bad", "Average", "Good"),
               ordered_result = T,
               right = F)

data <- cbind(wine.data, quality.factor)

```

```{r Heatmap}
library(reshape2)
cor.data <- cor(wine.data[c(1:12)])

melted.cor.data <- melt(cor.data)

ggplot(data = melted.cor.data, aes(x=Var1, y=Var2, fill=value)) + 
  geom_tile() + 
  labs(x=NULL, y=NULL) +
  scale_fill_gradient2(low="orange",high="red",mid="white") +
  theme(axis.text.x = element_text(angle = 90))
```

```{r Scatterplot}
scatter.plotter <- function(attribute1, attribute2) {
    ggplot(data, aes_string(x=attribute1, y=attribute2, col=data$quality.factor)) + 
     geom_point(size=0.5, alpha=0.3) +
    scale_color_manual(values = c("red", "dodgerblue2", "darkgreen")) +
    xlab(attribute1) + ylab(attribute2) + labs(color="Quality score") +
    geom_smooth(size=0.5,method='lm',formula=y~x)
}

df <- scatter.plotter("density", "fixed.acidity")
pf <- scatter.plotter("pH", "fixed.acidity")
cf <- scatter.plotter("citric.acid", "fixed.acidity")
cv <- scatter.plotter("citric.acid", "volatile.acidity")
tf <- scatter.plotter("total.sulfur.dioxide", "free.sulfur.dioxide")

data$total.sulfur.dioxide <- log2(data$total.sulfur.dioxide)
data$free.sulfur.dioxide <- log2(data$free.sulfur.dioxide)

tfl <- scatter.plotter("total.sulfur.dioxide", "free.sulfur.dioxide")

grid.arrange(arrangeGrob(df, top="A"),arrangeGrob(pf, top="B"))
grid.arrange(arrangeGrob(cf, top="A"),arrangeGrob(cv, top="B"))
grid.arrange(arrangeGrob(tf, top="A"), arrangeGrob(tfl, top="B"))
#ggplot(data, aes(x=quality.factor, y=log.total.sulfur)) + geom_boxplot() +
#  ggtitle("Boxplot with log transformated total sulfur")
```

To see if there is any correlation in the attributes I made a heatmap with the correlation values. If a box is filled up with a red or orange color, it meens that there will be a (small) correlation. The sets of attributes that stand out the most are: density-fixed.acidity, pH-fixed-acidity, citric.acid-fixed-acidity, citric.acid-volatile.acidity and total.sulfur.dioxide-free.sulfur.dioxide. These are the attributes that show the most correlation. To make this more clear I made scatterplot of these attribute sets.  In these scatterplots can bee seen that they are not all correlated that much, but there may be some correlation. It is good to know which attributes correlate with each other, so I can decide if I will use them in the further research.

```{r Histogram}
# funcion to create histogram of all attributes
make.hist <- function(attribute, att, num) {
  p <- ggplot(data, aes_string(x=attribute, col=quality.factor)) +
            geom_histogram() +
            ggtitle(num) +
            scale_color_manual(values = c("red", "dodgerblue2", "green")) +
            xlab(att) + labs(color="Quality score")
  print(p)
}


# Histograms of all attributes
p <- list()
for (i in c(1:10)) {
  p[[i]] <- make.hist(data[,i], codebook[i,2], LETTERS[i])
}

# Arragne all the plots in two multiplots
do.call(grid.arrange, p[1:5])
do.call(grid.arrange, p[5:10])


```
To check the distribution of the attributes in the data, I made a function that creates a histogram with the counts of a certain value. As can been seen, most of the attributes are normally distributed, but some do not. Especially the residual sugar, chlorides and sulfates counts show that there are weird outliers that should get some more attention.

## 25-09-2019
## PCA

```{r PCA}
library(ggbiplot)

# log transform the data and create factor of quality scores
log.data <- log2(data[,c(1:11)])
log.data$quality.factor <- factor(data[,13])
log.data <- log.data[!is.infinite(rowSums(log.data[,c(1:11)])),]

# Create princpipal components
wine.pca <- prcomp(log.data[,c(1:11)],
                 center = TRUE,
                 scale. = TRUE)

# Create PCA plot
g <- ggbiplot(wine.pca, obs.scale = 1, var.scale = 1, 
              groups = log.data[,"quality.factor"], ellipse = FALSE, 
              circle = TRUE, alpha = 0.2) +
              scale_color_manual(name="Variety",
                                 values=c("red", "dodgerblue2", "darkgreen")) +
              geom_point(aes(colour=log.data[,"quality.factor"]), size=0.3) +
              theme(legend.direction = 'horizontal',
                    legend.position = 'top')

# Replace the cicle layers to the forground
g$layers <- c(g$layers, g$layers[[1]], g$layers[[2]], g$layers[[4]])

print(g)
```


I made a PCA plot to see if I can find some patterns in the quality scores. There is quite a lot of variance in the data. But it is good to see that all the attributes point their own way in the PCA plot. This means that they all have a specific influence on the variance of the data. In the first sight it looks like alcohol, fixed acidity, sulphates and citric acid have a good influence because there are higher quality scores in that direction. On the other hand, volatily acidity, free sulfur dioxide and total sulfur dioxide decrease the quality score.

# 28-09-2019
Because the attributes free sulfur dioxide and total sulfur dioxde correlate strongly, I decided to remove the free sulfur dioxdie attribute from the dataset. The quality attribute is not needed anymore because I recoded it to the factors Bad, Average and Good. Before the data can go into weka.

# added 6-11-2019
After some struggling with the java application I decided to go back to the base of the datafile. The application didn't work and I couldn't get it working. I think it was about having 3 different class attribute values (Bad,Average,Good), so in the next chunk I divided the data in just good and bad, below 6 is marked as "Bad" and above 6 is marked as "Good". I used this newly generated data to run the application. Surprisingly it worked fine! Even if I tried to run it with more than 2 instances. I still don't know exactly why it didn't work with more than 2 labels.

```{r change class attribute 2}
# reformat the class attribute
quality.gb <- cut(wine.data$quality, 
               breaks = c(0,6,10), 
               labels = c("Bad", "Good"),
               ordered_result = T,
               right = F)

data.gb <- cbind(wine.data, quality.gb)
```

```{r remove column}
# remove free sulfur dioxide and quality column
data.gb$free.sulfur.dioxide <- NULL
data.gb$quality <- NULL

# getting rows with bad values
i <- which(data.gb$residual.sugar>4|data.gb$chlorides>0.2|data.gb$sulphates>1.0)

# removing rows 
data.normalized <- data.gb[-i, ]
rownames(data.normalized) <- NULL

# Function for normalization
scale_min_max <- function(x) (x - min(x)) / (max(x) - min(x))

# Perform normalization
data.normalized[1:10] <- lapply(data.normalized[1:10], scale_min_max)

# write the new clean csv file
write.table(data.normalized, file="clean_data.csv", sep=",", row.names = FALSE, col.names = TRUE)
```

# Investigate performance of Machine Learning
To make a good model for this research I used Weka, this is a software tool where different algorithms can be tested and changed to get the best possible algorithm for your dataset. Especially the Weka experimenter tool is very useful for comparing multiple algorithms. To use the experimenter there need to be uploaded a datafile in arff of csv format. After that, there can be chosen various algorithms in the iteration control menu. Then the classifications need to be performed and this can be done in the Run tab that is on top of the program. In this menu there only need to be clicked on the start button and then it will start performing the algorithms one by one. After all algorithms are performed, it will output if there were any errors. If there were no errors, the test accuracy scores can be printed in the analyse tab. In this menu you need to click on experiment and perform test to see the results.
Another useful Weka tool is the Weka explorer, in this explorer, a file can be uploaded in arff or csv format. When the file is loaded, it can be used to classify instances in the classify tab. In this tab various algorithms can be chosen to predict instances. It will output information such as: the accuracy, the true positive rate, the false negative rate, etc. These are all rates that are calculated by the program and are based on how well the algorithm performed. It will also output a confusion matrix in which the correctly and incorrectly classified instances are shown.  

## Confusion matrix

| Predicted Average | Predicted Good | Predicted Bad |
|---|:--:| --:|---:|
| TA | FG1 | FB2 | Actual Average |
| FA1 | TG | FB2 | Actual Good |
| FA1 | FG2 | TB | Actual Bad |

TG = True Good
TA = True Average
TB = True Bad
FA = False Average
FG = False Good
FB = False Bad

Looking at the confusion matrix, 
I make "false good 2" instances more important because the last thing wine producers want is to put a bad wine on the market, while the algorithm predicts it will be a good one. If the algorithm predicts it will be a bad wine, but it is actually a good wine, it is less worse. It is ultimately about the producer not wanting to put a bad wine on the market.
In the outcomes of the weka algorithms, the TP.rate is an important value to look at. This value tells how accurate the algorithm is with predicting bad instances. 
To find the best algorithms I looked at the TP.rate of the bad instances and at the accuracy of correctly classified instances. The algorithms that I tested are: ZeroR, OneR, Naive bayes, J48 and RandomForest. The algorithm that worked best are: Naive bayes, J48, and RandomForest. Naive bayes has an accuracy significantly lower than ZeroR which is actually very bad, but it has a TP.rate of 0.180, which was the highest of all algorithms. J48 has an accuracy of 83.9% correctly classified instances and a TP.rate of 0.100. J48 is therefore the best overall algorithm if one algorithm may be used. The RandomForest algorithm gives the highest accuracy of correctly classified instances, which is 88.34, but the TP.rate of the bad instances is 0.0. This means that this algorithm is not good enough in itself.


|Dataset     |              (1) rules.Ze | (2) rules | (3) bayes | (4) trees | (5) trees |
|-------------|---------------------------|-----------|--------------|--------|---------|
|clean_data    |           (100)   83.70 |   84.17  |   80.95 *  | 84.75  |   88.34 v |

(1) rules.ZeroR '' 48055541465867954
(2) rules.OneR '-B 6' -3459427003147861443
(3) bayes.NaiveBayes '' 5995231201785697655
(4) trees.J48 '-C 0.25 -M 2' -217733168393644444
(5) trees.RandomForest '-P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1' 1116839470751428698

To create the best possible algorithm for this dataset I used majority voting. Majority voting is a rule in algorithms that uses the 'vote' of multiple algorithms for the right classification. So if most of the algorithms predict it is a bad wine, it assumes that it is a bad wine. It combines multiple algorithms to get the best result. The algorithms I used in the majority voting are: Naive bayes, J48 and RandomForest. I chose for these because they all have their pros and cons, and by trying to combine them into one algorithm, it should give a high accuracy of correctly classified instances but it should also have a high TP.rate of the bad instances.

|Dataset            |       (1) trees.J4 | (2) meta. |
|----------------|------------------------|----------|
|clean_data      |         (100)   84.75 |   86.73 v |
  
(1) trees.J48 '-C 0.25 -M 2' -217733168393644444  
(2) meta.Vote '-S 1 -B \"trees.RandomForest -P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1\" -B \"bayes.NaiveBayes \" -B \"trees.J48 -U -M 2\" -R MAJ
-637891196294399624  

```{r}
# Loading results from Random forest algorithm
results.RF <- read.table("Results_weka/ResultsRF.csv", header=T, sep=";")
# Loading results from Naive bayes algorithm
results.NB <- read.table("Results_weka/ResultsNB", header=T, sep=";")
# Loading results from J48 algorithm
results.J48 <- read.table("Results_weka/ResultsJ48.csv", header=T, sep=";")
```


## Confusion matrix of majority voting algorithm with 3 class attribute values
    a    b    c   <-- classified as  
 1104   66    6 |    a = Average  
   86   93    0 |    b = Good  
   45    0    5 |    c = Bad  
   

# 6-11-2019
Because I changed the class attribute from 3 to 2 values, I also changed the algorithm to use. Majority voting was not the best option anymore, Random Forest with a changed seed to 10, came out as the best algorithm. this gave an accuracy of 82.4199, which was a lot lower than with 3 class attributes, but if I could get it working that way then that's okay. Random Forest was still the algorithm with the highest accuracy.  
As can be seen in the table with accuracy scores below, all the algorithms performed significantly better than ZeroR. The reason that ZeroR works so much worse than with 3 different attribute labels is because the data is much better distributed. With 3 different labels the "average label" was much more present than the "bad" and "good" labels. With 2 different labels it is much more divided among the labels.
The v behind the accuracy scores tells that the algorithm is significantly better than ZeroR.

|Dataset       |            (1) rules.Ze | (2) rules |(3) bayes | (4) trees | (5) trees |
|--------------|--------------------------|-----------|---------|-----------|---------|
|clean_data    |           (100)   53.74 |   69.80 v  | 74.30 v |  74.81 v  | 82.19 v |

  
Key:  
(1) rules.ZeroR '' 48055541465867954  
(2) rules.OneR '-B 6' -3459427003147861443  
(3) bayes.NaiveBayes '' 5995231201785697655  
(4) trees.J48 '-C 0.25 -M 2' -217733168393644444  
(5) trees.RandomForest '-P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 10' 1116839470751428698  
  
## Confusion matrix of Random Forest algorithm with 2 class attribute values
   a   b   <-- classified as  
 525 125 |   a = Bad  
 122 633 |   b = Good  


